package com.sap.ci.exception

final case class ApplicationFailureException (private val message: String = "",
                                       private val cause: Throwable = None.orNull
                                      ) extends Exception(message, cause)
