package com.sap.ci.utils

import com.amazon.deequ.checks.{Check, CheckLevel}
import com.amazon.deequ.constraints.ConstrainableDataTypes
import com.sap.ci.conf.AppConf
import net.liftweb.json.{DefaultFormats, parse}
import org.apache.spark.sql.catalyst.util.DateTimeUtils
import com.sap.ci.utils.Constants.{CONSTRAINABLE_DATATYPES, IS_COMPLETE}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.joda.time.DateTime
import scala.reflect.api.JavaUniverse
import scala.reflect.runtime.universe
import scala.reflect.runtime.universe.{ImplDef, Quasiquote, runtimeMirror, typeOf}
import scala.tools.reflect.ToolBox

object CommonUtils {

  implicit val formats: DefaultFormats.type = DefaultFormats
  val ru: JavaUniverse = scala.reflect.runtime.universe
  val rm: ru.Mirror = ru.runtimeMirror(getClass.getClassLoader)
  val mirror: universe.Mirror = runtimeMirror(getClass.getClassLoader)
  val tb: ToolBox[universe.type] = ToolBox(mirror).mkToolBox()

  case class DQ1(column: String, dqMethod: String, arg1: Option[String])

  case class DQ2(column: String, dqMethod: String, arg1: List[String])

  case class DQ3(
                  column: String,
                  dqMethod: String,
                  arg1: List[String],
                  arg2: String,
                  arg3: String
                )

  def time(f: => Unit): Long = {
    val start = System.nanoTime()
    f
    val end = System.nanoTime()
    (end - start) / 1000 / 1000
  }

  def getCurrentDate: String = {
    val date = DateTime.now()
    val format = "yyyy-MM-dd"
    date.toString(format)
  }

  def readDeequeConstraints(deequeConf: String): Check = {
    var _check = Check(CheckLevel.Error, "Data Validation Check")
    val bufferedSource = scala.io.Source.fromFile(deequeConf)
    for (line <- bufferedSource.getLines) {
      val cols = line.split('|').map(_.trim)
      val constraint = cols(2)
      val pattern = ".([A-Za-z]+)\\((.*?)\\)".r
      val pattern(method, args) = constraint
      val modArgs = args.replaceAll("\"", "")
      if (method.contains(IS_COMPLETE)) {
        _check = _check.isComplete(modArgs)
      }
    }
    bufferedSource.close()
    _check
  }

  implicit class DQChain(_check: Check)(implicit conf: AppConf) {

    def dqMethod1: Check = {
      val dq = scala.io.Source.fromFile(conf.dqschemaPath1)
      val dqList = parse(dq.mkString).extract[List[DQ1]]

      dqList
        .foldLeft(_check) { (check, dq) => {
          val instanceMirror = rm.reflect(check)
          val methodSymbol =
            ru.typeOf[Check].decl(ru.TermName(dq.dqMethod)).asMethod

          val method = {
            instanceMirror.reflectMethod(methodSymbol)
          }
          dq.arg1 match {
            case Some(x) if x.contains(CONSTRAINABLE_DATATYPES) =>
              val arg1 = ConstrainableDataTypes.values.find(_.toString == x.split("\\.")(1)).get
              val arg2 = Check.IsOne
              val arg3 = None
              method(dq.column, arg1, arg2, arg3)
                .asInstanceOf[Check]
            case _ => None
              method(dq.column, None)
                .asInstanceOf[Check]
          }
        }
        }
    }

    def dqMethod2: Check = {
      val dq = scala.io.Source.fromFile(conf.dqschemaPath2)
      val dqList = parse(dq.mkString).extract[List[DQ2]]

      dqList
        .foldLeft(_check) { (check, dq) => {
          val instanceMirror = rm.reflect(check)
          val methodSymbol = ru
            .typeOf[Check]
            .decl(ru.TermName(dq.dqMethod))
            .asTerm
            .alternatives
            .find(s =>
              s.asMethod.paramLists.map(_.map(_.typeSignature)) == List(
                List(typeOf[String], typeOf[Array[String]])
              )
            )
            .get
            .asMethod
          val method = instanceMirror.reflectMethod(methodSymbol)
          method(dq.column, dq.arg1.toArray)
            .asInstanceOf[Check]
        }
        }
    }

    def dqMethod3: Check = {

      val dq = scala.io.Source.fromFile(conf.dqschemaPath3)
      val dqList = parse(dq.mkString).extract[List[DQ3]]

      dqList
        .foldLeft(_check) { (check, dq) => {
          val instanceMirror = rm.reflect(check)
          val methodSymbol = ru
            .typeOf[Check]
            .decl(ru.TermName(dq.dqMethod))
            .asTerm
            .alternatives
            .find(s =>
              s.asMethod.paramLists.map(_.map(_.typeSignature)) == List(
                List(
                  typeOf[String],
                  typeOf[Array[String]],
                  typeOf[Function[Double, Boolean]],
                  typeOf[Option[String]]
                )
              )
            )
            .get
            .asMethod
          val function = s"def apply(x: Double): Boolean = x ${dq.arg2}"
          val functionWrapper = "object FunctionWrapper { " + function + "}"
          val functionSymbol = tb.define(tb.parse(functionWrapper).asInstanceOf[tb.u.ImplDef])
          val func = tb.eval(q"$functionSymbol.apply _").asInstanceOf[Double => Boolean]

          val method = instanceMirror.reflectMethod(methodSymbol)
          method(
            dq.column,
            dq.arg1.toArray,
            func,
            Some(dq.arg3)
          ).asInstanceOf[Check]
        }
        }
    }
  }

  def renameFile(path: String)(implicit
                               sparkSession: SparkSession, conf: AppConf) = {

    val hadoopConf = sparkSession.sparkContext.hadoopConfiguration
    val fs = FileSystem.get(hadoopConf)
    val files = fs.listFiles(new Path(path), true)
    var counter = 1
    val fileModName = "spend_data"
    while (files.hasNext) {

      val currFilePath = files.next().getPath
      val currFileParentPath = currFilePath.getParent

      if (!currFilePath.toString.contains("_SUCCESS")) {
        fs.rename(
          currFilePath, new Path(
            s"${currFileParentPath.toString}/${fileModName}-snappy-${counter}.orc"
          )
        )
        counter += 1
      }
    }
  }

  def stripSpaceFromColumns(df: DataFrame): DataFrame = {

    var newDf = df
    for (col <- df.columns) {
      newDf = newDf.withColumnRenamed(
        col,
        col.replaceAll("\\s", "_").replaceAll("\\\\", "")
      )
    }
    newDf
  }
}