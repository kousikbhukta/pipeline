package com.sap.ci.utils

object MonthName extends Enumeration {

  type MonthName = Value
  val Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec = Value

}
