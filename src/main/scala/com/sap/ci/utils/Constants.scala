package com.sap.ci.utils

object Constants {
  val SPARK_ORC_CONF = "spark.sql.orc.impl"
  val SPARK_ORC_CONF_VAL = "native"
  val LOCAL_MASTER = "local[*]"
  val SPARK_SQL_SHUFFLE_PARTITIONS = "spark.sql.shuffle.partitions"
  val PARTITION_NUMBER = "50"
  val LOCAL_EXECUTION_MODE = "LOCAL"

  //DEEQUE
  val IS_COMPLETE = "isComplete"
  val CONSTRAINABLE_DATATYPES = "ConstrainableDataTypes"
  val METRIC_FILE_NAME = "Metrics.json"
  val TAG_NAME = "PIPELINE"

  // Metric related
  val FINAL_RECORDS_COUNT = "final_record_count"
  val LOAD_DATE = "Load_Date"
  val CORRUPT_RECORDS_COUNT = "corrupt_records_count"
  val VALID_RECORDS_COUNT = "valid_records_count"
  val DEDPLICATED_RECORD_COUNT = "deduplicated_record_count"
}
