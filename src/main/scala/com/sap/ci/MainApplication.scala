package com.sap.ci

import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import com.sap.ci.service.{Metrics, Pipeline, SessionProvider}
import com.sap.ci.utils.CommonUtils
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import pureconfig.ConfigSource
import pureconfig.generic.auto._


object MainApplication extends Logging {

 def main(args: Array[String]): Unit = {

   System.setProperty("hadoop.home.dir","C:/hadoop")
   
   logInfo("Application begin...")
   
   implicit val conf: AppConf =
     ConfigSource.file(args(0)).loadOrThrow[AppConf]
     implicit val sparkSession: SparkSession = SessionProvider(
       conf
     ).getSparkSession

     try {

       val elapsedTime:Long = CommonUtils.time(Pipeline.executePipeline)
       if (
         Metrics.longMetrics.get(
           "deduplicated_record_count"
         ) == Metrics.longMetrics.get("final_record_count")
       ) {
         logInfo(s"published long metrics : ${Metrics.longMetrics.toString()}")
         logInfo("Final records are matched")
       } else {
         logInfo(s"published long metrics : ${Metrics.longMetrics.toString()}")
         logInfo("Final records are not matched")
         throw ApplicationFailureException()
       }
     }
     catch {
       case e: Exception =>
         throw ApplicationFailureException(e.getMessage, e.getCause)
     } finally {
       sparkSession.catalog.clearCache()
       sparkSession.close()
     }
 }
}
