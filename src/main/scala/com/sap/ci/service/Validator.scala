package com.sap.ci.service

import com.amazon.deequ.{VerificationResult, VerificationSuite}
import com.amazon.deequ.checks.{Check, CheckLevel}
import com.amazon.deequ.repository.ResultKey
import com.amazon.deequ.repository.fs.FileSystemMetricsRepository
import com.amazon.deequ.VerificationResult.checkResultsAsDataFrame
import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import com.sap.ci.utils.CommonUtils.DQChain
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import com.sap.ci.utils.Constants.{METRIC_FILE_NAME, TAG_NAME}

import java.io.File

object Validator extends Logging {

  /**
   *
   * @param dataFrame
   * @param sparkSession
   * @param conf
   */
  def deequeValidator(dataFrame: DataFrame)(implicit sparkSession: SparkSession, conf: AppConf): Unit = {
    try {

      logInfo("DQ Validation..")
      val _check = Check(
        CheckLevel.Error,
        "Data Validation Check"
      ).dqMethod1.dqMethod2

      val metricsFile = new File(conf.dqValidationPath, METRIC_FILE_NAME)
      val repository = FileSystemMetricsRepository(sparkSession, metricsFile.getAbsolutePath)
      // Create indexed for each set of computed
      val resultKey = ResultKey(System.currentTimeMillis(), Map("tag" -> TAG_NAME))

      val verificationResult: VerificationResult = {
        VerificationSuite()
          .onData(dataFrame)
          .addCheck(_check)
          .useRepository(repository)
          .saveOrAppendResult(resultKey)
          .run()
      }
      val resultDataFrame = {
        checkResultsAsDataFrame(sparkSession, verificationResult)
      }

      logInfo("Writing dq Validation..")
      resultDataFrame
        .coalesce(1)
        .write
        .mode(SaveMode.Append)
        .option("header", "true")
        .csv(conf.dqValidationPath)

    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }
}