package com.sap.ci.service

import scala.collection.mutable

object Metrics {
  
  val longMetrics: mutable.Map[String, Long] = scala.collection.mutable.Map[String, Long] ()
  val stringMetrics: mutable.Map[String, String] = scala.collection.mutable.Map[String, String] ()

}
