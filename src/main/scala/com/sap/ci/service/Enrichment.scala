package com.sap.ci.service

import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{DataFrame, SparkSession}

object Enrichment extends Logging {
  
  
  
  def transformationDF(dataFrame: DataFrame)(implicit sparkSession: SparkSession): DataFrame = {
    try  {
      logInfo("Transformation starts..")
      dataFrame
    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }

  def copyProcessedFile()(implicit config: AppConf): Unit ={
    val conf = new Configuration()
    val hdfs = FileSystem.get(conf)
    val srcPath = new Path(config.srcPath)
    val destPath = new Path(config.fileCheckPath)

    // Deleting destination path before loading
    hdfs.delete(destPath, true)

    org.apache.hadoop.fs.FileUtil.copy(
      srcPath.getFileSystem(conf),
      srcPath,
      destPath.getFileSystem(conf),
      destPath,
      false,
      conf
    )
  }

  def filesCheck()(implicit sparkSession: SparkSession, conf: AppConf): Boolean ={

    var destFile: Set[String] = Set()
    var file1: String = ""
    var file2: String = ""

    val hadoopConf = sparkSession.sparkContext.hadoopConfiguration
    val fs = FileSystem.get(hadoopConf)

    val inputFiles1 = fs.listFiles(new Path(conf.srcPath), true)
    while (inputFiles1.hasNext) {
      val fileName = inputFiles1.next().getPath.getName
      file1 = fileName.toString

    }

      val destFiles = fs.listFiles(new Path(conf.fileCheckPath), true)
      while (destFiles.hasNext){
        val fileName = destFiles.next().getPath.getName
        destFile += fileName.toString

  }
    val inputFiles: Set[String] = Set(file1)

    if (inputFiles.equals(destFile)) {
    System.exit(1)
      false
    }
    else {
      true
    }
  }

}
