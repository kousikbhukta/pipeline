package com.sap.ci.service

import com.sap.ci.conf.AppConf
import com.sap.ci.utils.Constants
import com.sap.ci.utils.Constants.{LOCAL_MASTER, PARTITION_NUMBER, SPARK_ORC_CONF, SPARK_ORC_CONF_VAL, SPARK_SQL_SHUFFLE_PARTITIONS}
import org.apache.spark.sql.SparkSession

trait SessionProvider {
  def getSparkSession: SparkSession
}

object SessionProvider {

  private class LocalSparkSession(name: String) extends SessionProvider {
    override def getSparkSession: SparkSession =
      SparkSession
        .builder()
        .appName(name)
        .master(LOCAL_MASTER)
        .config(SPARK_ORC_CONF, SPARK_ORC_CONF_VAL)
        .config(SPARK_SQL_SHUFFLE_PARTITIONS, PARTITION_NUMBER)
        .getOrCreate()
  }

  private class ClusterSparkSession(name: String) extends SessionProvider {

    override def getSparkSession: SparkSession =
      SparkSession.builder().appName(name).getOrCreate()
  }

  /**
   *
   * @param config
   * @return
   */

  def apply(config: AppConf): SessionProvider = {

    if (config.executionMode.equalsIgnoreCase(Constants.LOCAL_EXECUTION_MODE)) {
      new LocalSparkSession(config.name)
    } else new ClusterSparkSession(config.name)
  }
}
