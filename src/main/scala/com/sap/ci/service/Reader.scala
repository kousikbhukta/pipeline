package com.sap.ci.service

import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import org.apache.spark.deploy.history.LogInfo
import org.apache.spark.internal.Logging
import org.apache.spark.sql.types.{DataType, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}

object Reader extends Logging {

  /**
   *
   * @param srcPath Source path defined in conf file
   * @param srcSchema Source schema path defined in conf file
   * @param delim File separated by |
   * @param sparkSession
   * @param conf
   * @return
   */
  
  def readCSV(srcPath: String, srcSchema:String, delim: String)(implicit sparkSession: SparkSession, conf:AppConf): DataFrame = {
    logInfo("CSV reading starts..")
    try {
      
      val jsonSchema = sparkSession.read
        .text(srcSchema)
        .collect()
        .map(_.getString(0))
        .mkString(" ")
      val schema = DataType.fromJson(jsonSchema).asInstanceOf[StructType]
      
      val df = sparkSession.read
        .option("delimiter", delim)
        .option("header", "true")
        .schema(schema)
        .csv(srcPath)
      df
    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }
}
