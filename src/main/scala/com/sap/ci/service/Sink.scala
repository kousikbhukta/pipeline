package com.sap.ci.service
import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import com.sap.ci.utils.Constants
import org.apache.spark.internal.Logging
import org.apache.spark.sql.functions.current_date
import org.apache.spark.sql.types.{DataType, StructType}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel

object Sink extends Logging {

  /**
   * @param dataFrame Provide DataFrame
   * @param sparkSession
   * @param conf
   */

  def hdfsSink(dataFrame: DataFrame)(implicit sparkSession: SparkSession, conf: AppConf): Unit = {
    logInfo("Writing final result in the destination path")
    try {

      val jsonSchema = sparkSession.read
        .text(conf.ouputSchemaPath)
        .collect()
        .map(_.getString(0))
        .mkString(" ")
      val schema = DataType.fromJson(jsonSchema).asInstanceOf[StructType]

      val newDataFrame = sparkSession.createDataFrame(dataFrame.rdd, schema)
      newDataFrame.persist(StorageLevel.MEMORY_AND_DISK_SER_2)

      Metrics.longMetrics += Constants.FINAL_RECORDS_COUNT -> newDataFrame.count()

//      val finalDataFrame = newDataFrame.withColumn(Constants.LOAD_DATE, current_date())
      newDataFrame
        .coalesce(conf.coalesceFactor)
        .write
        .option("header", "true")
        .mode(SaveMode.Overwrite)
//        .partitionBy(Constants.LOAD_DATE)
//        .partitionBy("YEAR".toLowerCase(), "MONTH".toLowerCase())
        .csv(conf.destPath)

    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }
}
