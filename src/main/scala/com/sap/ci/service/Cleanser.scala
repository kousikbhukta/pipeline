package com.sap.ci.service

import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import com.sap.ci.utils.Constants
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{DataFrame, SparkSession}

object Cleanser extends Logging {

  /**
   *
   * @param dataFrame
   * @param fileName
   * @param sparkSession
   * @return
   */
  def cleanCorruptRecords(dataFrame: DataFrame, fileName: String)(implicit sparkSession: SparkSession): DataFrame = {

    try {

      import sparkSession.implicits._
      if (dataFrame.columns.contains("_corrupt_records")) {

        val invalidDataFrame = dataFrame.filter($"_corrupt_records".isNotNull)
        invalidDataFrame.cache()
        Metrics.longMetrics += Constants.CORRUPT_RECORDS_COUNT + "_" + fileName -> invalidDataFrame.count()
        invalidDataFrame.unpersist()

        val validDataFrame = dataFrame.filter($"_corrupt_records".isNull)
        validDataFrame.cache()
        Metrics.longMetrics += Constants.VALID_RECORDS_COUNT + "_" + fileName -> validDataFrame.count()
        validDataFrame.unpersist()
        validDataFrame

      } else {
        Metrics.longMetrics += Constants.VALID_RECORDS_COUNT + "_" + fileName -> dataFrame.count()
        dataFrame
      }
    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }

  /**
   *
   * @param valiDataFrame
   * @param sparkSession
   * @param conf
   * @return
   */

  def dropDuplicates(valiDataFrame: DataFrame)(implicit sparkSession: SparkSession, conf: AppConf): DataFrame = {

    try {

      logInfo("Removing duplicates records from the datasets..")
      val df = valiDataFrame.distinct()

      df
    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }
}
