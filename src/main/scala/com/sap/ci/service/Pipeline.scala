package com.sap.ci.service

import com.sap.ci.conf.AppConf
import com.sap.ci.exception.ApplicationFailureException
import com.sap.ci.utils.{CommonUtils, Constants}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

object Pipeline extends Logging {

  def executePipeline(implicit sparkSession: SparkSession, conf: AppConf): Unit = {

    try {
      val df = Reader.readCSV(conf.srcPath, conf.inputSchemaPath, conf.delim)
      val df2 = Enrichment.transformationDF(df)
      Validator.deequeValidator(df2)

      Metrics.longMetrics += Constants.DEDPLICATED_RECORD_COUNT -> df2.count()

      Sink.hdfsSink(df2)
      CommonUtils.renameFile(conf.destPath)
      Enrichment.copyProcessedFile()
    } catch {
      case e: Exception =>
        throw ApplicationFailureException(e.getMessage, e.getCause)
    }
  }
}
