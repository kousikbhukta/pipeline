package com.sap.ci.conf

case class AppConf(
                    name: String,
                    executionMode: String,
                    srcPath: String,
                    destPath: String,
                    inputSchemaPath: String,
                    ouputSchemaPath: String,
                    delim: String,
                    dqValidationPath: String,
                    dqschemaPath1: String,
                    dqschemaPath2: String,
                    dqschemaPath3: String,
                    coalesceFactor: Int,
                    fileCheckPath: String
                  )
