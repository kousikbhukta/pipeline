#!/usr/bin/env bash

application_name=$1
master=$2
deploy_mode=$3
class_name=$4
spark_jar=$5
spark_conf=$6
log_file_base_dir=$7
queue_name=$8
user_name=$9
base_local_path=${10}
spark_log_file=${log_file_base_dir}/${application_name}/${date +"%Y-%m-%d"}.log

cd ${base_local_path}
retry=1
status=1
while [[ $retry -le 3 && $status !=0 ]]
do
    export SPARK_MAJOR_VERSION=2
    /usr/hdp/current/spark2-client/bin/spark-submit --verbose \
    --name ${application_name} \
    --master ${master} \
    --deploy-mode ${deploy_mode} \
    --num-executors 2 \
    --executor-cors 2 \
    --executor-memory 2G \
    --queue ${queue_name} \
    --conf spark.driver.memory=4G \
    --conf spark.dynamicAllocation.enabled=false \
    --conf "spark.executor.extraJavaOptions=-XX:MaxPermSize=1024m -XX:PermSize=256m -XX:+UseG1GC" \
    --conf "spark.driver.extraClassPath=lib/*" \
    --conf "spark.executor.extraLibraryPath=lib/*" \
    --class ${class_name} \
    ${spark_jar} ${spark_conf} >> ${spark_log_file} 2>&1

    status=$?

    if [[ $status != 0 ]]; then
      echo "ERROR: occurred while running spark code on attempt $retry " >> ${spark_log_file}
      sleep 4m
      retry=$((retry + 1))
      else
        echo "Spark Submit ran successfully"
      fi

    done

 if [[ $status !=0 ]]; then
   echo "ERROR: occurred while running spark job after running for $((retry-1)) attempts." >> ${spark_log_file}
   exit 1
 fi