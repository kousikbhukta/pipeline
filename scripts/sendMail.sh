#! /usr/bin/env bash

set -vx

maitTo=$1
instance_date-$2
attachment_dir=$3
log_dir=$4
app_name=$5

log_file=${log_dir}/${app_name}-$(date + "&Y-%m-%d").log

rm -R -f ${log_dir}/data/ >> ${log_file} 2>&1


from="Team name"
sub=""
emailBody ="
HI All,
------
-----
Thanks,

"
echo "${emailBody}" | /usr/bin/mailx -r "${from}" -s "${sub}" -a "${attachment_dir}/report/Test.csv" \
"${maitTo}"